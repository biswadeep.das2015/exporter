# Use an official Python runtime as a parent image
FROM python:3.8-slim

# Set the working directory in the container
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY exporter.py /app/exporter.py

# Install any needed packages specified in requirements.txt
RUN pip install prometheus_client

# Run exporter.py when the container launches
CMD ["python", "exporter.py"]